import yaml
import os

class Config:

    def __init__(self):

        self.filepath = os.path.join(os.sys.path[0], "config.yaml")
        self.yaml_loader()

    # Loads the yaml file and creates it if it doesn't exist
    def yaml_loader(self):
        try:
            with open(self.filepath, 'r') as file_descriptor:
                self.data = yaml.load(file_descriptor)
            return self.data
        except FileNotFoundError or FileExistsError:
            self.create_yaml()

    def create_yaml(self):
        filepath = os.path.join(os.sys.path[0], "config.yaml")

        # This is what will be in your YAML file

        yaml_dictionary = {}

        # --------------------------------------

        with open(filepath, 'w') as file_descriptor:
            yaml.dump(yaml_dictionary, file_descriptor)
        self.yaml_loader()

# Debugging for testing YAML output
'''
data = Config().yaml_loader()
print(data)
'''